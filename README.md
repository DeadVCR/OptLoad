Are your players complaining because of the Epic Dubstep [BASS BOOSTED] "music" you're playing on your loading screen?

Have you ever wished for a way for your players to opt in or out of music playback on your loading screen?

Look no further, OptLoad is for you!

### What is OptLoad?
OptLoad is a sleek, stylish loading screen that allows your players to opt in or out of music playback while they're connecting. It also features random music selection, as well as random image selection to keep your players entertained. Everything is fully custom, so change it all to your liking!

### Preview
![A preview of OptLoad](https://i.imgur.com/iNY1gkC.png "OptLoad")

Or a live demo [here](http://prosecutorgodot.com/?steamid=76561198125727691)

### Requirements

1. A brain
2. A webserver with PHP and MySQL Server installed
3. [MySQLOO](https://github.com/FredyH/MySQLOO) installed

### Installation

1. Create a MySQL database and allow external connections (please ask me if you need help with this)
2. Download and install [MySQLOO](https://github.com/FredyH/MySQLOO)
3. Put the contents of the "ADDON" folder into your garrysmod/addon folder
4. Configure `lua/autorun/server/sv_optload.lua` and change the MySQL configuration to match the details of your MySQL server
5. Put the contents of the "WEB" folder onto your web server
6. Open `index.php` and change the configuration to match the details of your MySQL server. Change `$orgname` to match the name of your server
7. Get your Steam API key from [here](https://steamcommunity.com/dev/apikey) and put it in the `$apikey` variable.
8. Edit your Garry's Mod config and add the following line, matching your domain. `sv_loadingurl "http://example.com/?steamid=%s&mapname=%m"`
9. Reboot your server and enjoy!

### Configuration

To add your own music simply drag your songs into the `music` directory on your webserver. Music **NEEDS TO BE IN .MP3 FORMAT**

Same steps go for new images, simply put them in the `images` directory. These can be `.jpg`, `.png` or `.gif`.

The logo image and background image are located in the `bg` directory. Simply change them to whatever you want.

### Usage

To opt in to loading screen music, type `!loadingmusic in` while in game. To opt out, type `!loadingmusic out`. that's it!