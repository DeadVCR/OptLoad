require("mysqloo")

////////////CONFIG////////////
local DATABASE_HOST = "127.0.0.1" //Host
local DATABASE_PORT = 3306 //Port. This is the default so don't change this unless you changed your MySQL config
local DATABASE_NAME = "" //Database Name
local DATABASE_USERNAME = "" //Your MySQL username
local DATABASE_PASSWORD = "" //Your MySQL password
//////////////////////////////


local function connectToDatabase()
    databaseObj = mysqloo.connect(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME, DATABASE_PORT)
    databaseObj.onConnected = function() print("Music database linked") end
    databaseObj.onConnectionFailed = function(err) print(err) end
    databaseObj:connect()
end

connectToDatabase()
local function changeOpt(ply, io)
    local query = databaseObj:query("INSERT INTO `playsong` (`name`, `playMusic`) VALUES ('"..ply.."', '"..io.."') ON DUPLICATE KEY UPDATE `playMusic` = '"..io.."'")
    query:start()
end

local function askMusic(ply,text,team)
    if string.lower(text) == "!loadingmusic" then
        ply:PrintMessage(HUD_PRINTTALK, "Please type !loadingmusic in to opt into loading screen music or !loadingmusic out to opt out")
    elseif string.lower(text) == "!loadingmusic out" then
        changeOpt(ply:SteamID64(), 0)
        ply:PrintMessage(HUD_PRINTTALK, "Successfully opted out of loading screen music!")
    elseif string.lower(text) == "!loadingmusic in" then
        changeOpt(ply:SteamID64(), 1)
        ply:PrintMessage(HUD_PRINTTALK, "Successfully opted in to loading screen music!")
    end
end



hook.Add("PlayerSay", "askMusic", askMusic)