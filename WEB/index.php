<?php
#Config stuff. You need to change this.
$orgname = "Resonance Servers"; //The organization name that shows on the loading screen
$servername = "127.0.0.1"; //MySQL server address
$username = ""; //MySQL username
$password = ""; //MySQL password
$dbname = ""; //MySQL database name
$apikey = ""; //Steam API key. Get it here: https://steamcommunity.com/dev/apikey


#Don't touch anything beyond this line unless you know what you're doing.
$link = mysqli_connect($servername, $username, $password, $dbname);
$imgDir = 'images/';
$imgSrc = scandir($imgDir);
$i = rand(2, sizeof($imgSrc)-1);
$musDir = 'music/';
$musSrc = scandir($musDir);
$m = rand(2, sizeof($musSrc)-1);
$name = $musSrc[$m];
$plname = 'Unknown';
$map = '';
$avatar = '';
$steamidget = $_GET['steamid'];
$steamid = filter_var($steamidget, FILTER_SANITIZE_STRING);

if (isset($_GET['mapname']))
    $map = $_GET['mapname'];

if (isset($_GET['steamid'])) {
    $data = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$apikey.'&steamids='.$_GET['steamid'];
    $f = file_get_contents($data);
    $arr = json_decode($f, true);
    if (isset($arr['response']['players'][0]['personaname']))
        $plname = $arr['response']['players'][0]['personaname'];
    if (isset($arr['response']['players'][0]['avatar']))
        $avatar = $arr['response']['players'][0]['avatar'];
    
}

$val = mysqli_query($link, 'CREATE TABLE IF NOT EXISTS `playsong` (name VARCHAR(128) NOT NULL , playMusic BOOLEAN NOT NULL, PRIMARY KEY(`name`))');
if($val !== FALSE)
{
  $sql = mysqli_query($link, 'SELECT `playMusic` FROM `playsong` WHERE (`name` = "'.$steamid.'")');
  $row = mysqli_fetch_assoc($sql);
  if($row['playMusic'] == 1)
  {
    $musicOptIn = true;
    echo '<audio id="audiothing" autoplay loop><source src="music/'.$musSrc[$m].'"></audio>';
  }
  else 
  {
    $musicOptIn = false;
  }
}
else
{
  echo "Something went wrong with MySQL";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="styleboys/style.css">
  <link rel="stylesheet" type="text/css" href="styleboys/bootstrap.min.css">

  <title>Loading Screen</title>
</head>
<body>
  <div class="bg-image"></div>
  <div class="logo">
    <img src="bg/logo.png"></img>
  </div>
  <div class="container">
    <h2><b>Welcome to <?php echo $orgname;?>!</b></h2>
    <h4>Please wait while everything downloads.</h4>
    <h4>While you wait, here's a random image from our collection of images!</h4>

    <img src="images/<?php echo $imgSrc[$i]; ?>" alt="" />
    
    <p>♫ Now playing: <b><?php print(str_ireplace(".mp3","",$name));?></b> ♫</p>
    <p>You have chosen <?php if(!$musicOptIn) { echo "NOT"; } ?> to opt into music.</p>
  </div>
  <div class="bottom-left well well-sm">
    <img src="<?php echo $avatar?>" alt="" class="pull-right img-circle">
    Welcome, <b><?php echo $plname?></b><br>
    We're currently playing on: <b><?php echo $map ?></b>

  </div>
<script>
  var audio = document.getElementById("audiothing");
  audio.volume = 0.2;
</script>	
</body>
</html>
